const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require("webpack");
var isProd = process.env.NODE_ENV === 'production';//return true or false
var cssDev = ['style-loader','css-loader','sass-loader'];
var cssProd =  ExtractTextPlugin.extract({ //extract text d disable
    //    use : ['style-loader','css-loader','sass-loader'], //extract text d disable HMR ONLY
            fallback : 'style-loader',//HMR
            use : ['css-loader','sass-loader'],//HMR
            publicPath : '/dist',//HMR
            allChunks: true,
        });

var cssConfig = isProd ? cssProd : cssDev;



module.exports = {
    mode : "development",
    entry : {
        app : './src/app.js',
        contact : './src/contact.js',
    },
    output : {
        path: path.resolve(__dirname, 'dist'),
        filename : '[name].bundle.js',
        //publicPath : '/dist/',//HMR

    },
    module : {
        rules : [
            {
                test : /\.scss$/,
                use : cssConfig
            }, 
            {
                test : /\.pug$/,
                use : ['raw-loader','pug-html-loader'],
            },
            { 
                test: /\.(jpe?g|gif|png|svg)$/i, 
                use:  [
                    //'file-loader?name=images/[name].[ext]',
                   'file-loader?name=[name].[ext]&outputPath=images/&publicPath=images/',
                    'image-webpack-loader'
                ]
            },
        ]
    },
    devServer: {
        contentBase: path.join( __dirname, 'dist'),
        compress: true,
        port: 9201,
        stats: 'minimal',
        open:true,
        hot:false,
       // watchContentBase: true,


    },
    
    plugins: [
      new HtmlWebpackPlugin({
        filename: 'index.html',
        minify : {
            collapseWhitespace : false,
        },
        hash : false,
        template: 'src/index.html'
      }), // Generates default index.html
      new HtmlWebpackPlugin({  // Also generate a test.html
        filename: 'test.html',
        minify : {
            collapseWhitespace : false,
        },
        hash : false,
        template: 'src/assets/test.pug'
      }),
      new HtmlWebpackPlugin({  // Also generate a test.html
        filename: 'test2.html',
        minify : {
            collapseWhitespace : false,
        },
        hash : false,
        template: 'src/assets/test2.html'
      }),
      new HtmlWebpackPlugin({  // Also generate a test.html
        filename: 'test3.html',
        minify : {
            collapseWhitespace : false,
        },
        hash : false,
        template: 'src/test3.html'
      }),

      new ExtractTextPlugin({
          filename : 'app.css',
          disable : !isProd,
          allChunks : true
      }),
      new webpack.HotModuleReplacementPlugin(),//HMR GLOBAL
      new webpack.NamedModulesPlugin(),//


    ]
}